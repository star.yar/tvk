# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_detect_post(self):
        """Test case for detect_post

        Detects if there is a defect
        """
        tvk_json = [List[object]()]
        response = self.client.open(
            '/beta/detect',
            method='POST',
            data=json.dumps(tvk_json),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
