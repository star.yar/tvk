import connexion
from connexion import exceptions
import six

from swagger_server import util

import numpy as np
import os

import torch
from torchvision.transforms.functional import to_pil_image

from nn.data_utils import normalize_image
from nn.infer import load_model, infer

# load models from specified ENV vars
env = os.environ
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
MODEL_ECI = env.get('MODEL_ECI')
MODEL_DEFECTS = env.get('MODEL_DEFECTS')
BATCH_SIZE = int(env.get('BATCH_SIZE'))

model_equipment = load_model(MODEL_ECI, device=DEVICE)
model_defects = load_model(MODEL_DEFECTS, device=DEVICE)


def detect_post(tvk_json):  # noqa: E501
    """Detects if there is a defect

     # noqa: E501

    :param tvk_json:
    :type tvk_json: List[]

    :rtype: List[object]
    """

    def parse_arrays(array_json):
        parsed_imgs_array = []
        for i, x in enumerate(array_json):
            img = np.array(x["tempArray"])
            h, w = img.shape

            # size check
            if h < 64 or w < 64:
                raise ValueError("Img should size be not lower than (64 x 64)")

            # processing of NaN arrays
            img_ = img.astype(float)
            num_nan = np.isnan(img_).sum()
            if num_nan > 0:
                if num_nan == np.prod(img_.shape):
                    raise ValueError(f"Img #{i} is absolutely NaN")
                else:
                    raise ValueError(f"Img #{i} is partially NaN")

            # skip all black image
            if np.min(img) == np.max(img):
                print(f"Img #{i} is black everywhere!")

            parsed_imgs_array.append(to_pil_image(normalize_image(img)))
        return parsed_imgs_array

    # parse termogramms
    try:
        imgs_array = parse_arrays(tvk_json)
    except KeyError:
        return exceptions.problem(400, "Bad input", f"Field `tempArray` is missing in one item of request array")
    except ValueError as e:
        return exceptions.problem(400, "Bad input", e.message())

    # classes = predict_classes(imgs_array)
    classes_all_prob, classes_map, classes_prob = infer(imgs_array, model_equipment)
    defects_all_prob, defects_map, defects_prob = infer(imgs_array, model_defects)
    return dict(classes=[classes_map.tolist(), classes_prob.tolist()],
                defects=[defects_map.tolist(), defects_prob.tolist()])
