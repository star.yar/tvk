import pickle as pkl
from datetime import datetime

import torch


def get_prescision(y_true, y_pred):
    from sklearn.metrics import precision_score

    return precision_score(y_true, y_pred, average='weighted')


def get_auc(y_true, y_score):
    from sklearn.metrics import roc_auc_score

    return roc_auc_score(y_true, y_score)


def pkl_save(o, path):
    import os
    rewrite = False
    if os.path.exists(path):
        rewrite = True if input(f"File {path} exists, replace? [y/N] ") in ["y", "Y", "yes", "YES"] else False
    if rewrite or not os.path.exists(path):
        with open(path, 'wb') as f:
            pkl.dump(o, f)
    else:
        print('Saving skipped...')


def pkl_load(path):
    with open(path, 'rb') as f:
        return pkl.load(f)


def get_datetime_now():
    return str(datetime.now()).split('.')[0] \
        .replace('-', '_') \
        .replace(' ', '_') \
        .replace(':', '_')


def log(epoch, logger, scalars, model, phase='train'):
    """
    Log metrics and model params

    Args:
        phase (str): one of ['train', 'val'], model logging will be done only for train phase
        epoch (int): train epoch
        logger (SummaryWriter): logger
        scalars (dict): pass in format: {metric_name: metric_val}
        model (nn.Module): torch model
    """

    for name, val in scalars.items():
        logger.add_scalar(name, val, epoch)

    if phase == 'train':
        # log model params
        for tag, value in model.named_parameters():
            tag = tag.replace('.', '/')
            logger.add_histogram(tag, get_item(value.data), epoch + 1)
            if value.grad is not None:
                # We need this since last layer is chosen ad-hoc.
                # For example, if we train classifier,
                # there will be no grad data at validator.
                logger.add_histogram(tag + '/grad', get_item(value.grad.data), epoch + 1)


def get_item(parameter: torch.Tensor):
    """Returns numpy array"""

    if parameter.requires_grad:
        parameter = parameter.detach()
    if parameter.is_cuda:
        parameter = parameter.cpu()
    return parameter.numpy()


def load_checkpoint(path, model, opt, device='cpu', load_opt=False):
    """
    Loads model wts & opt state (optionally)

    Args:
        path (str): Checkpoint path
        model (torch.nn.Module): Model instance to load weights for
        opt (torch.optim.Optimizer): Opt instance to load weights for (will be loaded only if `load_opt`)
        device (Union[str, torch.device]): `cuda:id` or `cpu`
        load_opt (bool): load opt or not
    """

    if path:
        checkpoint_data = torch.load(path, map_location=device)

        if 'epoch' in checkpoint_data.keys():
            # print last epoch number
            epoch = checkpoint_data['epoch']
            print(f'[DEBUG] Loaded model wts from epoch #{epoch}')

        # load model state
        model.load_state_dict(checkpoint_data['model_wts'])
        model.to(device)

        # load optimizer params if passed
        if load_opt:
            opt_state = checkpoint_data['optimizer']
            opt.load_state_dict(opt_state)
            print('[DEBUG] Loaded optimizer with params')


def checkpoint(epoch, path, model=None, optimizer=None, train_stats=None, best_model_wts=None):
    """Saves all data needed for recovering experiment:
        epoch; model; model weights; optimiser state; metrics; best model weights
    """
    torch.save({
        'epoch': epoch,
        'model_wts': model.state_dict(),  # saving model weights
        'optimizer': optimizer.state_dict() if optimizer is not None else optimizer,
        'metrics': train_stats,
        'best_model_wts': best_model_wts
    }, path)
