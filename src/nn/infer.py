from nn.utils import pkl_load, load_checkpoint
import torch


def load_model(experiment_name, stage=None, device=None):
    """Load experiment params"""
    from nn.model_utils import ModelBuilder

    if stage is None:
        import os
        stage = max([(f.replace(".pt", "").split("_")[1])
                     for f in os.listdir(f"../checkpoints/{experiment_name}")
                     if f.startswith("epoch")])

    args = pkl_load(f"../checkpoints/{experiment_name}/params.sav")
    tm = args['target_mapping']
    cw = args['class_weights']

    # init some common vars
    img_size = (224, 224)

    # загрузим предобученную модель
    model = ModelBuilder((1, *img_size), len(tm),
                         model_name=args['model'],
                         single_channel=not args['triple_channel'],
                         device=device)
    load_checkpoint(f"../checkpoints/{experiment_name}/epoch_{stage}.pt", model, None, device=device)

    model.img_size = img_size
    model.cw = cw
    model.tm = tm

    return model.eval()


def infer(inputs_array, model, batch_size=32, transformer=None):
    from nn.data_utils import build_transformers
    import numpy as np

    def gen_batches(l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]

    _pred = []
    if transformer is None:
        transformer = build_transformers('test', model.img_size)

    with torch.set_grad_enabled(False):
        for inputs in gen_batches(inputs_array, batch_size):
            inputs = torch.stack([transformer(x).to(next(model.parameters()).device) for x in inputs]).float()
            probas = model(inputs).softmax(1)
            _pred.append(probas.cpu())

    # squeeezing
    probas = torch.cat(_pred)

    top_n = 5 if len(model.tm) >= 5 else len(model.tm)
    pred_proba, pred_class = probas.topk(top_n, dim=1)

    # map targets
    mapped_pred_class = np.array([model.tm[pc]
                                  for batch in pred_class.tolist()
                                  for pc in batch]).reshape(len(inputs_array), -1)

    return probas, mapped_pred_class, pred_proba
