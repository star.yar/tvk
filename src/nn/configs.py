
def load_args():
    """
    Loads passes arguments

    Returns:
        arguments (dict):
    """

    import argparse

    # ad-hoc params creation
    import torch
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Parse arguments
    parser = argparse.ArgumentParser(description='DL Framework by @star.yar')
    parser.add_argument('--seed', default=42, type=int,
                        help='random seed (default: 42)')
    parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                        help='number of data loading workers (default: 4)')
    parser.add_argument('--model', default='', type=str, metavar='name',
                        help='What model to train.')

    # Optimization options
    parser.add_argument('--optim', default='', type=str, metavar='OPTIM_NAME',
                        help='one of [`rms`, `adam`] (default: None)')
    parser.add_argument('--epochs', default=1, type=int, metavar='N',
                        help='number of total epochs to run')
    parser.add_argument('--batch-size', default=32, type=int, metavar='N',
                        help='train batchsize (default: 32)')
    parser.add_argument('--lr', '--learning-rate', default=0.0001, type=float,
                        metavar='LR', help='initial learning rate')
    # parser.add_argument('--lr-decay', type=str, default='step',
    #                     help='mode for learning rate decay')
    parser.add_argument('--step', type=int, default=1,
                        help='interval for learning rate decay in step mode')
    # parser.add_argument('--schedule', type=int, nargs='+', default=[150, 225],
    #                     help='decrease learning rate at these epochs.')
    # parser.add_argument('--turning-point', type=int, default=100,
    #                     help='epoch number from linear to exponential decay mode')
    parser.add_argument('--gamma', type=float, default=0.98,
                        help='LR is multiplied by gamma on schedule.')
    parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                        help='momentum')
    parser.add_argument('--weight-decay', '--wd', default=1e-5, type=float,
                        metavar='W', help='weight decay (default: 1e-5)')

    # Device options
    parser.add_argument('--device', default=device, type=str,
                        help=f'Device name to train on (default: {device})')

    # Checkpoints
    parser.add_argument('-c', '--checkpoint', default='../checkpoints', type=str, metavar='PATH',
                        help='path to save checkpoint (default: ../checkpoints)')
    parser.add_argument('-cl', '--checkpoint-lp', action='store_true',
                        help='Use saved optim data (default: False)')
    parser.add_argument('-cf', '--checkpoint-freq', default=10, type=int, metavar='N',
                        help='Checkpoint freq (default: 10)')
    parser.add_argument('--resume', default='', type=str, metavar='PATH',
                        help='path to latest checkpoint (default: none)')
    parser.add_argument('--pretrained', default=True, type=bool, metavar='Bool',
                        help='Use pretrained model or not')
    parser.add_argument('--freeze', action='store_true', default=False,
                        help=f'Freeze feature extractor params (default: False)')

    # Loggers
    from nn.utils import get_datetime_now
    now = get_datetime_now()
    parser.add_argument('--logs-path', default='../logs', type=str, metavar='PATH',
                        help='Where to store logs (default: ../logs)')
    parser.add_argument('--micro-log', default=0, type=int, metavar='N',
                        help='Log every (n) iterations in epoch (default: 0)')
    parser.add_argument('--experiment-name', default=now, type=str,
                        help=f'Experiment name (default: {now})')

    # Project custom
    parser.add_argument('--data-path', default='../data/bin', type=str, metavar='PATH',
                        help='Where data is located (default: ../data/bin)')
    parser.add_argument('--img-dir', default='imgs', type=str, metavar='PATH',
                        help='Data dir name (default: imgs)')
    parser.add_argument('--tvk', default='05-TvkFiles.sav', type=str, metavar='PATH',
                        help='Tvk dump name (default: 05-TvkFiles.sav)')
    parser.add_argument('--build', action='store_true', default=False,
                        help=f'Flag for building initial files, such as target mapping and class weights'
                        '(default: False)')
    parser.add_argument('--triple-channel', action='store_true', default=False,
                        help=f'Flag for using three channeled input (default: False)')
    parser.add_argument('-om', '--old-mapping', action='store_true', default=False,
                        help=f'Flag for using old mapping scheme (default: False)')
    parser.add_argument('--classify', action='store_true', default=False,
                        help=f'Flag for doing classification (default: False)')

    parsed_args = vars(parser.parse_args())

    # create sub paths for quick ref in project
    data_path = parsed_args["data_path"]
    parsed_args['data_path'] = {
        'train_path': '{}/{}'.format(data_path, parsed_args['img_dir']),
        'val_path': '{}/{}'.format(data_path, parsed_args['img_dir']),
        'tvk_path': '{}/csv/{}'.format(data_path, parsed_args['tvk'])
    }

    return parsed_args


# prep env (unpacked for tracability)
args = load_args()
DEVICE = args['device']

CP_PATH = args['checkpoint']
RESUME = args['resume']
CP_EVERY = args['checkpoint_freq']
LOAD_OPT = args['checkpoint_lp']

LOGS_PATH = args['logs_path']
TRAIN_PATH = args['data_path']['train_path']
VAL_PATH = args['data_path']['val_path']
TVK_PATH = args['data_path']['tvk_path']

EPOCHS = args['epochs']

MICRO_LOG_EVERY = args['micro_log']
EXPERIMENT_NAME = args['experiment_name']

IMG_SIZE = (224, 224)
IMG_CHANNELS = 1

BATCH_SIZE = args['batch_size']
WORKERS = args['workers']

LR = args['lr']
MOMENTUM = args['momentum']
WEIGHT_DECAY = args['weight_decay']
STEP = args['step']
GAMMA = args['gamma']

SEED = args['seed']

BUILD_INITIALS = args['build']
MODEL_NAME = args['model']
PRETRAINED = args['pretrained']
SINGLE_CHANNEL = not args['triple_channel']
OPTIM = args['optim']
UNFREEZE = not args['freeze']

NEW_MAPPING = not args['old_mapping']
CLASSIF = args['classify']
