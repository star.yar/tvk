from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt

colors = [(_r / 255, _g / 255, _b / 255) for _r, _g, _b in
          [(245, 245, 245),
           (160, 160, 160),
           (125, 125, 125),
           (70, 70, 70),

           # black
           (0, 0, 0),

           # blue
           (100, 0, 255),
           # red
           (250, 90, 90),
           # yellow
           (255, 255, 150),
           # white
           (255, 255, 255)
           ]]

DEFAULT_CMAP = LinearSegmentedColormap.from_list('HV', colors, N=100)


def show_labels_hist(labels):
    plt.title("Num labels in batch")
    plt.hist(labels)
    plt.show()


def show_batch(batch, labels=None, fig_scale=2, cmap=None, cbar=False):
    """Show batch of data

    Args:
        batch (torch.Tensor): tensor of shape (B, C, H, W)
        labels (Union(list[str], torch.LongTensor)): batch elements labels
        fig_scale: figure scale
        cmap: plt color map
        cbar: Show cbar
    """

    from torchvision.transforms.functional import to_pil_image

    if batch.is_cuda:
        batch = batch.cpu()

    if cmap is None:
        cmap = DEFAULT_CMAP

    n_cols = 4
    n_rows = len(batch) // n_cols

    fig, axs = plt.subplots(n_rows, n_cols,
                            figsize=(n_cols * fig_scale, n_rows * fig_scale))

    for i, ax in enumerate(axs.flatten()):
        img = to_pil_image(batch[i])
        img_artist = ax.imshow(img, cmap=cmap)
        ax.set_axis_off()

        if labels is not None:
            if isinstance(labels, list):
                ax.set_title(labels[i])
            else:
                ax.set_title(labels[i].item())

        if cbar:
            fig.colorbar(img_artist, ax=ax)

    plt.show()


def show_img(img, title=None, cmap=None):
    if cmap is None:
        cmap = DEFAULT_CMAP

    plt.imshow(img, cmap=cmap)

    if title is not None:
        plt.title(title)
    plt.show()
