import os
import pickle as pkl

import numpy as np
import pandas as pd
from torch.utils.data import Dataset
from torchvision import transforms
from torchvision.transforms.functional import to_pil_image, pad

from nn.utils import pkl_load, pkl_save

CLASS_WEIGHTS_PATH = '../checkpoints/class_wts.sav'
TRAIN = 'train'
VAL = 'val'
TEST = 'test'


def normalize_image(image):
    """Функция для range нормализации (нужна перед переводом в PIL.Image)"""

    return (image * 255 / (image.max() - image.min())).astype('uint8')


def prep_tvk(tvk_path, aggregate_categories, classification):
    """Готовит разметку"""

    def drop_low_categories(df, filtration_col):
        """Функция убирает категории оборудования с малым кол-вом данных (меньше `threshold`)"""

        threshold = 2
        cat_cnt = df[filtration_col].value_counts()
        cat_cnt_low = cat_cnt[cat_cnt < threshold]
        print("Categories with popultaion < "
              f"{threshold}: {cat_cnt_low.index.values}")
        return df[~df[filtration_col].isin(cat_cnt_low.index)]

    def load_mapping(path, sheet_name):
        df = pd.read_excel(path, sheet_name=sheet_name)
        # берем только крупные категории
        df = df.loc[df['count'] >= 100, :].astype(str)
        return df

    def parse_mapping(df: pd.DataFrame):
        """Парсит df_mapping raw в удобный формат:
        tuple(equipment_id, equipment_element_id, equipment_name, equipment_element_name, new_class)
        """

        return [(int(ec_i), int(eec_i), ec_name, eec_name, idx)
                for idx, row in df.iterrows()
                for ec_i, eec_i, ec_name, eec_name in zip(row.equipment_category_id.split(','),
                                                          row.equipment_element_category_id.split(','),
                                                          row.equipment.split(','),
                                                          row.part_equipment.split(','))
                ]

    def collect_df(parsed_mapping):
        """Собирает DataFrame из парсинга"""

        return (pd
                .DataFrame(parsed_mapping, columns=['equipment_category_id', 'equipment_element_category_id',
                                                    'equipment', 'part_equipment', 'class'])
                .drop_duplicates()
                .set_index(['equipment_category_id', 'equipment_element_category_id'])
                )

    def map2class(x):
        """Лямбда для маппинга"""

        if x.equipment_category_id in df_mapping.index.levels[0]:
            if x.equipment_element_category_id in df_mapping.loc[x.equipment_category_id].index:
                classes = df_mapping.loc[x.equipment_category_id].loc[x.equipment_element_category_id, 'class']
                classes = np.unique(classes)
                if len(classes) > 1:
                    raise ValueError("Нужен маппинг 1 к 1.")
                return classes[0]
        return np.nan

    if aggregate_categories:
        # load and prep mapping
        df_mapping_raw = load_mapping(path='../data/Контрольные узлы_группы.xlsx', sheet_name='Union')
        mapping = parse_mapping(df_mapping_raw)
        df_mapping = collect_df(mapping)

        target_col = 'agg_class'
        df_tvk = pd.read_pickle(tvk_path)

        df_tvk[target_col] = df_tvk.apply(map2class, axis=1)  # map to new class
        df_tvk = df_tvk[df_tvk[target_col].notnull()]  # drop not mapped
        df_tvk = drop_low_categories(df_tvk, target_col)  # drop low cats
        df_tvk[target_col] = df_tvk[target_col].astype(int)  # convert class column to int
    else:
        target_col = 'equipment_category_id'
        df_tvk = pd.read_pickle(tvk_path)
        df_tvk = drop_low_categories(df_tvk, target_col)  # drop low cats

    if classification:
        cat_col = target_col
        target_col = 'defect'
        df_tvk[target_col] = (df_tvk.defect_stage > 1).astype(int)
        df_tvk['strata'] = df_tvk[[cat_col, target_col]].apply(lambda x: "_".join(x.astype(str)), axis=1)
    else:
        df_tvk['strata'] = df_tvk[target_col]

    # TODO: return mapping

    return df_tvk, target_col


class ImgDataset(Dataset):
    """Custom dataset for images"""

    def __init__(self, img_path, df_tvk, target_col, tm, transform=None):
        """Читает картинку из бинарника

        Args:
            img_path (str): путь к bin картинкам
            df_tvk (pd.DataFrame): TVK data frame
            target_col (str): target column
            transform (callable):
            tm (list[int]): target mapping - маппинг таргета (список истиных значений)
        """

        def _listdir(path):
            for f in os.listdir(path):
                if not f.startswith('.'):
                    yield f

        super().__init__()
        self._path = img_path
        self.targets = tm
        self.tvk = df_tvk
        self.target_col = target_col
        self.targets_mapping = tm
        self.transform = transform

        # выбор только тех записей разметки, для которых есть изображение
        folder_idx = set(_listdir(img_path))
        not_found_images = 0
        non_tvk = []
        for img_idx in self.tvk['id'].unique():
            if f"{img_idx}.sav" not in folder_idx:
                non_tvk.append(img_idx)
                self.tvk = self.tvk[self.tvk['id'] != img_idx].reset_index(drop=True)
                not_found_images += 1

        print(f"[DEBUG] Number of missing image files from TVK: {not_found_images}")
        print(f"[DEBUG] Their names: {non_tvk}")
        if self.tvk.empty:
            raise ValueError(f"[ERROR] Not found any images from TVK in {img_path}")

    @property
    def n_classes(self):
        return len(self.targets_mapping)

    def decode_label(self, x):
        return self.targets_mapping[x]

    def encode_label(self, x):
        return self.targets_mapping.index(x)

    def __getitem__(self, idx):
        """Возвращает изображение ассоциированное с записью TVK #idx"""

        tvk_row = self.tvk.iloc[idx]
        img_name = f"{tvk_row['id']}.sav"

        img = pkl_load(f"{self._path}/{img_name}")
        img = to_pil_image(normalize_image(img))

        if self.transform is not None:
            img = self.transform(img)

        return (img, img_name), self.encode_label(tvk_row[self.target_col])

    def __len__(self):
        return len(self.tvk)


def build_transformers(phase, desired_pic_size):
    from torchvision.transforms import Lambda

    def pad_image(img):
        aspect_raio = np.divide(*img.size)

        if aspect_raio == 1:
            pass
        if aspect_raio < 1:
            img = pad(img, ((img.size[1] - img.size[0]) // 2, 0), padding_mode='reflect')
        elif aspect_raio > 1:
            img = pad(img, (0, (img.size[0] - img.size[1]) // 2), padding_mode='reflect')

        return img

    alignment = Lambda(pad_image)
    normalize = transforms.Normalize(mean=[0.485],
                                     std=[0.229])

    tfs = {
        TRAIN: transforms.Compose([
            # alignment,
            transforms.RandomResizedCrop(desired_pic_size, scale=(.7, 1.), ratio=(1, 1)),
            transforms.ColorJitter(0.1, 0.1),
            transforms.RandomHorizontalFlip(0.5),
            transforms.RandomVerticalFlip(0.5),
            transforms.RandomRotation(45),
            transforms.ToTensor(),
            # normalize
        ]),

        VAL: transforms.Compose([
            # alignment,
            transforms.RandomResizedCrop(desired_pic_size, scale=(0.85, 1.0), ratio=(1, 1)),
            transforms.ColorJitter(0.05, 0.05),
            transforms.RandomRotation(15),
            transforms.ToTensor(),
            # normalize
        ]),

        TEST: transforms.Compose([
            alignment,
            transforms.Resize(desired_pic_size),
            transforms.ToTensor(),
            # normalize
        ]),
    }
    return tfs[phase]


def build_class_weights(dataset=None, save_cache_path='', load_cache_path='') -> list:
    """Counts class weights across given dataset

    Args:
        load_cache_path (str): path for loading precomputed weights
        save_cache_path (str): path for saving computed weights
        dataset (ImgDataset): dataset to build weights for
    """

    from nn.utils import pkl_load, pkl_save

    if load_cache_path:
        unique, counts = pkl_load(load_cache_path)
    else:
        df_counts = dataset.tvk[dataset.target_col].value_counts()
        unique = df_counts.index.values
        counts = df_counts.values

    if save_cache_path:
        pkl_save((unique, counts), save_cache_path)

    return 1 / (counts + 1)


def build_targets_mapping(df_tvk=None, target_col=None, save_path='', load_path=''):
    """Build target mapping from df_tvk

    Args:
        target_col (str): name of target column in df_tvk
        load_path (str): path for loading mapping
        save_path (str): path for saving mapping
        df_tvk (pd.DataFrame): data frame to build mapping over
    """

    if load_path:
        mapping = pkl_load(load_path)
    else:
        if target_col is None:
            raise ValueError("Pass target col when building mapping!")
        mapping = df_tvk[target_col].unique().tolist()
        if save_path:
            pkl_save(mapping, save_path)

    return mapping


def test_dataset():
    from torchvision.transforms.functional import to_pil_image
    from nn.configs import TRAIN_PATH, IMG_SIZE, TVK_PATH
    from nn.plotting import show_img

    args = pkl_load('../checkpoints/2019_08_18_17_04_48/params.sav')
    df_tvk, target_col = prep_tvk(TVK_PATH, not args['old_mapping'], args['classify'])
    dataset = ImgDataset(TRAIN_PATH, df_tvk, args['target_column'], args['target_mapping'],
                         transform=build_transformers(TRAIN, IMG_SIZE))
    (x, x_name), y = dataset[0]
    # _tmp = dataset.tvk.copy().reset_index(drop=True)
    # _idx = _tmp[_tmp.id == 8512].index.item()
    # (x, x_name), y = dataset[_idx]

    show_img(to_pil_image(x), f"Img {x_name}; Class {y}")


def test():
    test_dataset()


if __name__ == '__main__':
    # some tests
    test_dataset()
