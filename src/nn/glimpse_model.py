import sys
from nn.utils import pkl_load

# load arguments
cli_params = sys.argv
assert len(cli_params) > 1
model_path = cli_params[1]

# load experiment cli_params
model = pkl_load(model_path)

print('Model struct')
print('-' * 10)
print(model)
print()
