import torch
import torchvision
from torch import nn


class ModelBuilder(nn.Module):
    def __init__(self, input_shape, output_dim, model_name: str, use_pretrained=True, freeze=True,
                 single_channel=False, device=None):

        super().__init__()

        self.use_one_channel = single_channel
        self.in_dim, img_h, img_w = input_shape

        def rebuild_input(in_conv, init_fn=None):
            bias = any(in_conv.bias) if in_conv.bias is not None else False
            in_conv = torch.nn.Conv2d(self.in_dim, in_conv.out_channels,
                                      in_conv.kernel_size, in_conv.stride, in_conv.padding,
                                      in_conv.dilation, in_conv.groups,
                                      bias, in_conv.padding_mode)
            if init_fn == 'kaiming':
                nn.init.kaiming_uniform_(in_conv)

            return in_conv

        if model_name.startswith('resnet'):
            assert (img_h, img_w) == (224, 224)

            self.model = getattr(torchvision.models, model_name)(pretrained=use_pretrained)
            set_parameter_requires_grad(self.model, freeze)
            num_feats = self.model.fc.in_features
            self.model.fc = nn.Linear(num_feats, output_dim)
            if single_channel:
                self.model.conv1 = rebuild_input(self.model.conv1)

        elif model_name.startswith('vgg'):
            assert (img_h, img_w) == (224, 224)

            self.model = getattr(torchvision.models, model_name)(pretrained=use_pretrained)
            set_parameter_requires_grad(self.model, freeze)
            n_features = self.model.classifier[6].in_features
            self.model.classifier[6] = nn.Linear(n_features, output_dim)
            if single_channel:
                self.model.features[0] = rebuild_input(self.model.features[0])

        elif model_name.startswith('densenet'):
            assert (img_h, img_w) == (224, 224)

            self.model = getattr(torchvision.models, model_name)(pretrained=use_pretrained)
            set_parameter_requires_grad(self.model, freeze)
            n_features = self.model.classifier.in_features
            self.model.classifier = nn.Linear(n_features, output_dim)
            if single_channel:
                self.model.features.conv0 = rebuild_input(self.model.features.conv0)

        elif model_name == 'mobilenet':
            assert (img_h, img_w) == (224, 224)

            from nn.mobilenet import MobileNetV2
            self.model = MobileNetV2(n_class=output_dim, in_channels=1 if single_channel else 3)

        elif model_name == 'customnet1':
            self.model = CustomNet(input_shape, output_dim)

        else:
            raise ValueError(f'Model {model_name} not found')

        if device is not None:
            self.model = self.model.to(device)

    def forward(self, x):
        if not self.use_one_channel:
            x = torch.cat([x, x, x], dim=1)
        out = self.model(x)
        return out


class CustomNet(nn.Module):
    """
    Custom architecture
        - adapts for unknown input into fully connected block
    """

    def __init__(self, input_shape, output_dim):
        """

        Args:
            input_shape (tuple): (channels, width, height)
            output_dim (int): dim of output
        """
        super().__init__()

        self.in_dim, _, _ = input_shape
        self.out_dim = output_dim

        self.conv = nn.Sequential(
            nn.BatchNorm2d(self.in_dim),
            nn.Conv2d(self.in_dim, 16, 5, 2),
            nn.ReLU(),

            nn.MaxPool2d((2, 2), 2),
            nn.Conv2d(16, 32, 5, 2),
            nn.ReLU(),

            nn.MaxPool2d((2, 2), 2),
            nn.Conv2d(32, 64, 5, 2),
            nn.ReLU(),
        )

        out_shape, n_size = self._get_conv_output(input_shape)
        print(f"[DEBUG] Conv out shape: {out_shape}, n_features: {n_size}")

        self.fc = nn.Sequential(
            nn.Dropout(0.3),
            nn.Linear(n_size, 256),
            nn.ReLU(),

            # nn.Dropout(0.4),
            # nn.Linear(256, 128),
            # nn.ReLU(),

            nn.Linear(256, self.out_dim)
        )

    def _get_conv_output(self, shape):
        """
        Get conv output shape
        """
        batch_size = 1
        _input = torch.rand(batch_size, *shape)
        output_feat = self.conv(_input)
        n_size = output_feat.data.view(batch_size, -1).size(1)
        return output_feat.shape, n_size

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x


def init_weights(m):
    if type(m) == nn.Conv2d or type(m) == nn.Linear:
        nn.init.xavier_normal_(m.weight.data)


def get_trainable_parameters(model):
    return [p.numel() for p in model.parameters() if p.requires_grad]


def set_parameter_requires_grad(model, freeze_extractor):
    if freeze_extractor:
        for param in model.parameters():
            param.requires_grad = False
