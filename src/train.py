import copy
import time
import os

import torch
from sklearn.model_selection import train_test_split
from tensorboardX import SummaryWriter
from torch import nn
from torch import optim
from torch.utils.data import DataLoader

from nn.data_utils import build_transformers, ImgDataset, prep_tvk, build_class_weights, build_targets_mapping
from nn.model_utils import get_trainable_parameters, ModelBuilder
from nn.plotting import show_batch, show_labels_hist
from nn.utils import pkl_save, log, load_checkpoint, checkpoint


def train_model(model, criterion, optimizer, scheduler=None, data_loaders=None, loggers=None, sub_loggers=None):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_model_optimizer = copy.deepcopy(optimizer.state_dict())
    best_acc = 0.0

    # storing metrics
    train_stats = {x: []
                   for x in [TRAIN, VAL]}

    for epoch in range(EPOCHS):
        print('Epoch {}/{}'.format(epoch + 1, EPOCHS))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in [TRAIN, VAL]:
            # init metric vars on start
            i = 0
            seen = 0
            running_loss = .0
            running_corrects = 0

            if phase == TRAIN:
                if scheduler:
                    scheduler.step()  # works for everything except plateau
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            # Iterate over data.
            for (inputs, _), labels in data_loaders[phase]:
                inputs = inputs.to(DEVICE)
                labels = labels.to(DEVICE)

                if SHOW_BATCH:
                    show_batch(inputs, labels)
                    show_labels_hist(labels)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == TRAIN):
                    outputs = model(inputs)
                    loss = criterion(outputs, labels)
                    _, preds = torch.max(outputs, 1)

                    # backward + optimize only if in training phase
                    if phase == TRAIN:
                        loss.backward()
                        optimizer.step()

                # statistics
                seen += inputs.size(0)
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data).item()

                if sub_loggers is not None and i % MICRO_LOG_EVERY == 0:
                    log(epoch + i,
                        sub_loggers[phase],
                        {'Loss': running_loss / seen,
                         'Err': (1 - running_corrects / seen)},
                        model,
                        phase=phase)
                i += 1

            epoch_loss = running_loss / seen
            epoch_acc = running_corrects / seen

            # saving stats
            train_stats[phase].append((epoch, epoch_loss, epoch_acc))
            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))
            log(epoch,
                loggers[phase],
                {'Loss': epoch_loss, 'Err': 1 - epoch_acc},
                model,
                phase=phase)

            # deep copy the model
            if phase == VAL and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

        if epoch % CP_EVERY == 0:
            checkpoint(epoch, '{}/{}/epoch_{}.pt'.format(CP_PATH, EXPERIMENT_NAME, epoch),
                       model, optimizer, train_stats, best_model_wts)

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    optimizer.load_state_dict(best_model_optimizer)
    return model, optimizer, train_stats


def main():
    """
    Wrapper to training routine
    """

    micro_loggers = {x: SummaryWriter('{}/micro_{}_{}'.format(LOGS_PATH, EXPERIMENT_NAME, x))
                     for x in [TRAIN, VAL]} if MICRO_LOG_EVERY else None
    loggers = {x: SummaryWriter('{}/{}_{}'.format(LOGS_PATH, EXPERIMENT_NAME, x))
               for x in [TRAIN, VAL]}
    df_tvk, target_column = prep_tvk(TVK_PATH, aggregate_categories=NEW_MAPPING, classification=CLASSIF)

    if BUILD_INITIALS:
        target_mapping = build_targets_mapping(target_col=target_column, df_tvk=df_tvk, save_path=TARGET_MAPPING_PATH)
        print(f"[DEBUG] Built target mapping: {target_mapping}")
    else:
        target_mapping = build_targets_mapping(load_path=TARGET_MAPPING_PATH)
        print(f"[DEBUG] Loaded target mapping: {target_mapping}")

    dataset = {x: ImgDataset(path, tvk, target_column, target_mapping, transform=build_transformers(x, IMG_SIZE))
               for x, path, tvk in zip([TRAIN, VAL],
                                       [TRAIN_PATH, VAL_PATH],
                                       train_test_split(df_tvk, stratify=df_tvk['strata'],
                                                        test_size=0.1, random_state=SEED)
                                       )}
    data_loaders = {x: DataLoader(dataset[x], batch_size=BATCH_SIZE, shuffle=True,
                                  drop_last=False, num_workers=WORKERS)
                    for x in [TRAIN, VAL]}

    model = ModelBuilder((IMG_CHANNELS, *IMG_SIZE), dataset[TRAIN].n_classes, model_name=MODEL_NAME,
                         single_channel=SINGLE_CHANNEL, freeze=not UNFREEZE, use_pretrained=PRETRAINED, device=DEVICE)
    print(f'[DEBUG] Total trainable parameters: {sum(get_trainable_parameters(model)):,d}')

    if BUILD_INITIALS:
        weights = build_class_weights(dataset[TRAIN], save_cache_path=CLASS_WTS_PATH)
        print(f"[DEBUG] Built class weights: {weights}")
    else:
        weights = build_class_weights(load_cache_path=CLASS_WTS_PATH)
        print(f"[DEBUG] Loaded class weights: {weights}")

    criterion = nn.CrossEntropyLoss(weight=torch.tensor(weights, dtype=torch.float, device=DEVICE))

    if OPTIM == 'rms':
        optimizer = optim.RMSprop(model.parameters(), lr=LR,
                                  momentum=MOMENTUM, weight_decay=WEIGHT_DECAY)
        scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=STEP, gamma=GAMMA)
    elif OPTIM == 'adam':
        optimizer = optim.Adam(model.parameters(), weight_decay=WEIGHT_DECAY)
        scheduler = None
    else:
        raise ValueError(f"Optimiser {OPTIM} not found")

    # loading existing checkpoint
    load_checkpoint(RESUME, model, optimizer, device=DEVICE, load_opt=LOAD_OPT)

    # save session params (args are needed if interruption before final saving happens)
    args['target_column'] = target_column
    args['target_mapping'] = target_mapping
    args['class_weights'] = weights
    pkl_save(args, '{}/{}/params.sav'.format(CP_PATH, EXPERIMENT_NAME))

    model, optimizer, metrics = train_model(model, criterion, optimizer,
                                            scheduler=scheduler,
                                            data_loaders=data_loaders,
                                            loggers=loggers, sub_loggers=micro_loggers)

    # saving final results
    checkpoint(EPOCHS, '{}/{}/epoch_f.pt'.format(CP_PATH, EXPERIMENT_NAME), model, optimizer, metrics)


if __name__ == '__main__':
    from nn.configs import args, \
        DEVICE, \
        MODEL_NAME, \
        UNFREEZE, PRETRAINED, CP_PATH, RESUME, CP_EVERY, LOAD_OPT, \
        LOGS_PATH, TRAIN_PATH, VAL_PATH, \
        EPOCHS, \
        MICRO_LOG_EVERY, EXPERIMENT_NAME, \
        IMG_SIZE, \
        BATCH_SIZE, WORKERS, \
        OPTIM, LR, MOMENTUM, WEIGHT_DECAY, STEP, GAMMA, \
        TVK_PATH, SEED, IMG_CHANNELS, \
        BUILD_INITIALS, SINGLE_CHANNEL, \
        NEW_MAPPING, CLASSIF

    TRAIN = 'train'
    VAL = 'val'
    SHOW_BATCH = False
    CLASS_WTS_PATH = '../checkpoints/class_wts.sav'
    TARGET_MAPPING_PATH = '../checkpoints/targets_mapping.sav'

    # run baby, run
    print('Running with config')
    print('-' * 10)
    for key, val in sorted(args.items()):
        print('{}: {}'.format(key, val))
    print()

    # make dir for storing all experiment data
    os.mkdir(f'{CP_PATH}/{EXPERIMENT_NAME}')

    # main routine
    main()
