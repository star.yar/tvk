#!/bin/sh
echo "INFO: Remove container if exists"
if [ "$(docker ps -a | grep tvk)" ]; then
    sudo docker rm -f tvk
fi
echo "INFO: Building image"
sudo docker build -t tvk:0.1 .
echo "INFO: Starting container"
sudo docker run -d \
    --name=tvk \
    --restart=always \
    -v $(pwd)/../checkpoints/:/usr/app/checkpoints \
    --publish=$1:8080 \
    --env MODEL_ECI="2019_08_31_18_06_46" \
    --env MODEL_DEFECTS="2019_09_04_16_09_16" \
    --env BATCH_SIZE=32 \
    tvk:0.1
