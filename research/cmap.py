from matplotlib.colors import LinearSegmentedColormap

colors = [(_r/255, _g/255, _b/255) for _r, _g, _b in
          [(245, 245, 245),
           (160, 160, 160),
           (125, 125, 125),
           (70, 70, 70),
           
           # black
           (0, 0, 0),
           
           # blue
           (100, 0, 255),
           # red
           (250, 90, 90),
           # yellow
           (255, 255, 150),
           # white
           (255, 255, 255)
          ]]

cm = LinearSegmentedColormap.from_list('HV', colors, N=100)